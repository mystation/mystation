#!/bin/bash

# /mystation/boot/boot.sh

# Exit on error
set -e

# Variables
LOGS_DIR='/mystation/logs/'
BOOT_LOG_FILE='boot.log'

# --

# Turn on sango case LEDs
# THIS LINE MUST BE CHANGED!
sudo python /mystation/processes/leds-controller/lib/power.py

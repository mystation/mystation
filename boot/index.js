/*
 * MYSTATION PROCESSES BOOT
 */

const fs = require('fs')
const child_process = require('child_process')
const date = require('date-and-time')

// Global config
const GLOBAL_CONFIG = require('../config/config.json')
// Logger
const Logger = require('../utils/logger')
var logger = new Logger(GLOBAL_CONFIG.logger)
// Database manager
const DatabaseManager = require('../utils/database-manager')
var DBManager = new DatabaseManager(GLOBAL_CONFIG.database)
// Processes directory
const processesDirectory = '/mystation/processes'

const run = async function () {
  // Start
  logger.log(
    '========================================================\n' +
    '> MYSTATION BOOT [' + date.format(new Date(), 'YYYY-MM-DD HH:mm:ss') + '] -----------------\n' +
    '--------------------------------------------------------\n'
  )
  logger.log('--> Database connection --------------------------------\n')
  // Init database connection
  DBManager.init()
  // Get processes
  logger.log('----> Get processes ------------------------------------\n')
  var processes = await DBManager.query('SELECT * FROM ms_processes')
  // Loop on processes
  processes.forEach((process) => {
    logger.log(
      '------> LAUNCH: ' + process.name + '\n' +
      '      | ' + process.cmd + '\n'
    )
    child_process.exec(process.cmd)
  })
  // End
  logger.log(
    '----> Processes launched -------------------------------\n' +
    '--> Database communication finished --------------------\n' +
    '--------------------------------------------------------\n' +
    '> Booted [' + date.format(new Date(), 'YYYY-MM-DD HH:mm:ss') + '] -------------------------\n' +
    '========================================================\n'
  )
}

run()
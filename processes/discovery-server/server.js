// Global config
const GLOBAL_CONFIG = require('../../config/config.json')
// Database manager
const DatabaseManager = require('../../utils/database-manager')
var DBManager = new DatabaseManager(GLOBAL_CONFIG.database)

// Dgram
const dgram = require('dgram')
// Colors (for console return)
var colors = require('colors')

// Create UDP4 (IPv4) dgram socket
var socket = dgram.createSocket('udp4')

/**
 * Start listening for discover
 * @param {string} application - The application name (label to identify messages)
 * @param {string} version - The MyStation version -> must match 'X.X.X' where X are integers
 * @param {string} [serverPort='7777'] - The server port (local). Default: 7777
 * @param {string} [loggerLabel='[>MyStation!]'] - The prefix to log
 * @return {void}
 */
function startDiscovery (application, version, serverPort = '7777', loggerLabel = '[>MyStation!]') {
  // Error event
  socket.on('error', err => {
    console.error(loggerLabel + ' Mother Box Discovery Server: ' + ('ERROR'.red) + ' - ', err)
    socket.close()
  });
  // Listening event -> emitted when socket.bind()
  socket.on('listening', () => {
    console.log(loggerLabel + ' Mother Box Discovery Server: ' + ('STARTED'.green) + ' [' + socket.address().address + ':' + serverPort + ']')
  })
  // Message event
  socket.on('message', (msg, rinfo) => {
    let JSONmsg
    // Try to parse JSON message
    try {
      JSONmsg = JSON.parse(msg.toString('utf8'))
      if (!JSONmsg.application) {
        throw new Error('Error')
      }
    } catch (err) {
      console.error(`${loggerLabel} Mother Box Discovery Server: ` + (`ERROR`.red) + ` message received from ${rinfo.address}:${rinfo.port} not in readable JSON format.`)
      return
    }
    // Check application name
    if (JSONmsg.application.name !== 'MYSTATION') {
      console.error(`${loggerLabel} Mother Box Discovery Server: ` + (`IGNORE`.yellow) + ` message received from ${rinfo.address}:${rinfo.port} : don't match application name.`)
      return
    }
    // Check version
    if (JSONmsg.application.version !== '1.0.0') {
      console.error(`${loggerLabel} Mother Box Discovery Server: ` + (`RETURN ERROR`.yellow) + ` message received from ${rinfo.address}:${rinfo.port} : don't match version.`)
      socket.send(Buffer.from(JSON.stringify({
        error: 'BAD_VERSION'
      })),
      rinfo.port,
      rinfo.address)
      return
    }
    // Check MyStation key
    if (JSONmsg.application.key !== '0000-0000-0000-0000') {
      console.error(`${loggerLabel} Mother Box Discovery Server: ` + (`RETURN ERROR`.yellow) + ` message received from ${rinfo.address}:${rinfo.port} : don't match mystation key.`)
      socket.send(Buffer.from(JSON.stringify({
        error: 'BAD_APPLICATION_KEY'
      })),
      rinfo.port,
      rinfo.address)
      return
    }

    // All checking passed -> Send response
    console.info(`${loggerLabel} Mother Box Discovery Server: ` + (`SEND RESPONSE`.cyan) + ` to ${rinfo.address}:${rinfo.port}`)
    // socket.send(Buffer.from([0xaf, 0xcf]), rinfo.port, rinfo.address)
    socket.send(Buffer.from(JSON.stringify({
      address: rinfo.address,
      port: rinfo.port
    })),
    rinfo.port,
    rinfo.address)
  })
  // Bind
  socket.bind(serverPort)
}

const run = async function () {
  // Init database connection
  DBManager.init()
  // Get system config (in JSON format)
  var systemConfig = await DBManager.query('CALL getSystemData(\'JSON\')', 'proc') // Second argument to 'proc'
  systemConfig = JSON.parse(systemConfig[0].json_object) // First raw -> 'json_object' alias column name
  // Start server
  startDiscovery(systemConfig.APPLICATION, systemConfig.VERSION, systemConfig.UDP_SERVER_PORT, systemConfig.LOGGER_LABEL)
}

run()
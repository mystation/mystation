const child_process = require('child_process')

var Parser = require('../../utils/parser')

var parser = new Parser({ 
  only: ['GetProcessesData', 'ProcessOutput'],
  plugins: {
    ProcessOutput: {
      label: '[>MyStation!]'
    }
  }
}) // To use only needed plugins

// Get MyStation processes
// var psOutput = child_process.execSync('ps -eo pid,user,comm,args | grep "\\/mystation\\/"', {
//   encoding: 'utf8'
// })
var psOutput = child_process.execSync('ps -eo pid,user,comm,args', {
  encoding: 'utf8'
})

var processes = parser.parse(psOutput)['GetProcessesData']

var watchers = {
  discoveryServer: null
}

processes.forEach((process) => {
  if (/\/mystation\/processes\/ms_discovery_server\/server.js/.test(process.args)) {
    // Strace the PID, limit to 254 characters and get 'write' output property
    watchers.discoveryServer = child_process.spawn('strace', [('-p' + process.pid), '-s254', '-e', 'trace=write'])
    watchers.discoveryServer.stdout.on('data', (data) => {
    //   let outputRE = /write\([0-9]+(.*), [0-9]+\)/
    //   let toto = watchers.discoveryServer.match(outputRE)
    //   console.log(toto)
    })
    watchers.discoveryServer.stderr.on('data', (data) => {
      var outputREstart = /write\([0-9]+,[\s]+(.*)/
      var outputREend = /(",[\s]+[0-9]+)$/ // to match ", 84)
      let output = `${data}`.match(outputREstart)
      if (output !== null) {
        output = output[1].replace(outputREend, '')
        output = output.substr(1) // To remove the first '"'
        output = output.substr(-2) === '\\n' ? output.substr(0, output.length - 2) : output // Clear \n at the end
      }
      console.log(parser.parse(output).ProcessOutput)
    })
  }
})
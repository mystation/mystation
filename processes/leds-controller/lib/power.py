import RPi.GPIO as GPIO

# GPIO BCM numbering
GPIO.setmode(GPIO.BCM)

# Closing the warnings when compiling the code
GPIO.setwarnings(False)

# Defining the LEDs pins
RED_PIN = 26
GREEN_PIN = 6
BLUE_PIN = 5

# Defining pins as output
GPIO.setup(RED_PIN, GPIO.OUT)
GPIO.setup(GREEN_PIN, GPIO.OUT)
GPIO.setup(BLUE_PIN, GPIO.OUT)

# Turn on green and blue pins
GPIO.output(RED_PIN, GPIO.LOW)
GPIO.output(GREEN_PIN, GPIO.HIGH)
GPIO.output(BLUE_PIN, GPIO.HIGH)

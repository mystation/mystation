const fs = require('fs')

class Logger {
  /**
   * Logger constructor
   * @param {Object} options - Options
   * @param {Object} options.defaultTarget - Default target file to write logs
   */
  constructor (options) {
    this.defaultTarget = options.defaultTarget
  }

  log (str) {
    fs.appendFileSync(this.defaultTarget.path, str)
  }
}

module.exports = Logger
const plugins = require('./plugins/index.js')

class Parser {
  /**
   * Parser constructor
   * @param {Object} options - Options
   * @param {Array} options.only - A list of plugins (names)
   * @param {Object} [options.plugins={}] - Config per plugin
   */
  constructor (options) {
    this.plugins = {}
    // If only is empty -> add all plugins
    if (options.only.length === 0) {
      this.plugins = plugins
    } else {
      // Else, add only into 'only' array
      var tmp;
      Object.keys(plugins).forEach((plugin) => {
        tmp = null
        tmp = options.only.find((pluginName) => {
          return pluginName === plugin
        })
        // If plugin exists
        if (tmp) {
          // Add to this.plugins
          this.plugins[plugin] = plugins[plugin]
          // Also add options
          if (options.plugins[plugin]) {
            this.plugins[plugin].options = options.plugins[plugin]
          } else {
            this.plugins[plugin].options = {}
          }
        }
      })
    }
    // Add all functions
    this.detect = this.mergeDetect()
  }

  mergeDetect () {
    var tmp = {}
    // For each plugins -> add detect function to 'queue'
    Object.keys(this.plugins).forEach((plugin) => {
      // plugin.detect must be a function taking str, options in parameter
      tmp[plugin] = this.plugins[plugin].detect
    })
    return tmp
  }

  parse (str) {
    var tmp = {}
    Object.keys(this.detect).forEach((plugin) => {
      var response = this.detect[plugin](str, this.plugins[plugin].options)
      if (response) {
        tmp[plugin] = response
      }
    })
    return tmp
  }
}

module.exports = Parser
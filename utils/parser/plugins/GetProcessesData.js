function matchLabel (str) {
    if (str) {
      var re = /([0-9]+)[\s]([A-z]+)[\s]+([A-z]+)[\s]+(.*)/ // (1:PID)blanks(2:USER)blanks(3:COMM)blanks(4:ARGS)
      var processes = []
      var lines = str.split('\n')
      lines.forEach(psLine => {
        if (re.test(psLine)) {
          processes.push({
            pid: psLine.match(re)[1],
            user: psLine.match(re)[2],
            command: psLine.match(re)[3],
            args: psLine.match(re)[4]
          })
        }
      })
      return processes.length === 0 ? null : processes 
    }
    return null
  }
  
  module.exports = {
    // Must have 'detect' method 
    detect: function (str, options) {
      var response;
      // If detect label in the 
      if (response = matchLabel(str)) {
        return response
      } else {
        return false
      }
    }
  }
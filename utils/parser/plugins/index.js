const GetProcessesData = require('./GetProcessesData.js')
const ProcessOutput = require('./ProcessOutput.js')

module.exports = {
  GetProcessesData,
  ProcessOutput
}
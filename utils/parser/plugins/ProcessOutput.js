const escapeStringRegexp = require('escape-string-regexp');

function matchLabel (str, label) {
  var re = new RegExp('^(' + escapeStringRegexp(label) + ')(.*)$')
  var message = null
  if (re.test(str)) {
    var message = str.match(re)
  }
  return {
    label: label,
    match: message
  }
}

module.exports = {
  // Must have 'detect' method
  detect: function (str, options) {
    var response;
    // If detect label in the 
    if (response = matchLabel(str, options.label)) {
      return response
    } else {
      return false
    }
  }
}
var mysql = require('mysql')

class DatabaseManager {
  /**
   * Logger constructor
   * @param {Object} options - Options
   * @param {string} options.host - Database server host name
   * @param {number} options.port - Database server port
   * @param {string} options.user - User name
   * @param {string} options.password - User password
   * @param {string} options.database - Database name
   */
  constructor (options) {
    this.host = options.host || 'localhost'
    this.port = options.port || 3306
    this.user = options.user || 'admin'
    this.password = options.password || 'admin'
    this.database = options.database || 'mystation'
  }

  init () {
    // Connect database
    this.connection = mysql.createConnection({
      host: this.host,
      port: this.port,
      user: this.user,
      password: this.password,
      database: this.database,
    })
  }

  query(str, type = 'proc') {
    return new Promise((resolve, reject) => {
      var conn = this.connection
      conn.connect((err) => {
        if (err) {
          reject(err)
        }
        conn.query(str, function (err, result, fields) {
          if (err) {
            reject(err)
          }
          var results = []
          if (type !== 'proc') {
            result.forEach((raw, index) => {
              let tmp = {}
              fields.forEach((field, index) => {
                if (fields[index]) {
                  tmp[field.name] = result[index][field.name]
                }
              })
              results.push(tmp)
            })
          } else { // Else -> procedure
            results = result[0]
          }
          resolve(results)
        })
      })
    })
  }
}

module.exports = DatabaseManager
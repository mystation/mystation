#!/bin/bash

# /mystation/install/update.sh

# Exit on error
set -e

# Variables
VERSION=$(cat /mystation/VERSION)
LOGS_DIR='/mystation/logs/'
UPDATE_LOG_FILE="update_$(date '+%Y%m%d%H%M%S').log"

# --

# Function to log in console AND log file
function log {
    end_color="\e[0m"
    case "$2" in
        blue)
            open_color="\e[36m"
            ;;
        red)
            open_color="\e[31m"
            ;;
        green)
            open_color="\e[92m"
            ;;
        yellow)
            open_color="\e[33m"
            ;;
        *)
            open_color="\e[0m"
            ;;
    esac
    # Output to console
    echo -e "$open_color$1$end_color"
    # Output to log file (whitout colors)
    echo -e "$1" >> $LOGS_DIR/$UPDATE_LOG_FILE
}

log "---------------------------------" blue
log "| MyStation update script       |" blue
log "---------------------------------" blue
log "| > Start $(date '+%Y-%m-%d %H:%M:%S')   |" blue
log "---------------------------------" blue

# Update UI
log "- Update UI"
(cd /mystation/server && php artisan ui:update) > >(tee -a $LOGS_DIR/$UPDATE_LOG_FILE) 2>&1
log "  > OK" green

# Update permissions
log "- Apply permissions"
sudo chown -R admin:www-data /mystation/server > >(tee -a $LOGS_DIR/$UPDATE_LOG_FILE) 2>&1
sudo chmod -R 770 /mystation/server > >(tee -a $LOGS_DIR/$UPDATE_LOG_FILE) 2>&1
log "  > OK" green

# Update version in database
log "- Update database 'system_data' table: version"
mysql -uadmin -padmin -e "UPDATE mystation.system_data SET value='$VERSION' WHERE system_key='VERSION';" > >(tee -a $LOGS_DIR/$UPDATE_LOG_FILE) 2>&1
sudo chmod -R 770 /mystation/server > >(tee -a $LOGS_DIR/$UPDATE_LOG_FILE) 2>&1
log "  > OK" green

# Check php7.3-gd extension
log "- Install php GD extension if not installed"
if apt -qq list php7.3-gd 2>/dev/null | grep -wo installed; then
  log "php7.3-gd already installed" yellow
else
  sudo apt-get install -y php7.3-gd > >(tee -a $LOGS_DIR/$UPDATE_LOG_FILE) 2>&1
fi
log "  > OK" green

log "Update finished. Please reboot the system." yellow

log "---------------------------------" blue
log "| > End $(date '+%Y-%m-%d %H:%M:%S')     |" blue
log "---------------------------------" blue

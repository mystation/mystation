#!/bin/bash

# /mystation/install/install.sh

# Exit on error
set -e

# Variables
LOGS_DIR='/mystation/logs/'
INSTALL_LOG_FILE='install.log'

# --

# Check if log file exists
if [ -f "$LOGS_DIR/$INSTALL_LOG_FILE" ]; then

    echo -e "\e[33mInstallation has already been completed\e[0m"
    exit 1

else 

    # Function to log in console AND log file
    function log {
        end_color="\e[0m"
        case "$2" in
            blue)
                open_color="\e[36m"
                ;;
            red)
                open_color="\e[31m"
                ;;
            green)
                open_color="\e[92m"
                ;;
            yellow)
                open_color="\e[33m"
                ;;
            *)
                open_color="\e[0m"
                ;;
        esac
        # Output to console
        echo -e "$open_color$1$end_color"
        # Output to log file (whitout colors)
        echo -e "$1" >> $LOGS_DIR/$INSTALL_LOG_FILE
    }

    log "---------------------------------" blue
    log "| MyStation installation script |" blue
    log "---------------------------------" blue
    log "| > Start $(date '+%Y-%m-%d %H:%M:%S')   |" blue
    log "---------------------------------" blue

    # Run init command
    log "- Run init script"
    (cd /mystation && npm run init) > >(tee -a $LOGS_DIR/$INSTALL_LOG_FILE) 2>&1
    log "  > OK" green

    # Database
    log "- Create database"
    (cd /mystation/database && bash /mystation/database/init.sh admin admin) > >(tee -a $LOGS_DIR/$INSTALL_LOG_FILE) 2>&1
    log "  > OK" green

    # Apache conf
    log "- Copy apache2 VirtualHost configuration"
    sudo cp /mystation/install/apache2/conf/sango.conf /etc/apache2/sites-available/sango.conf > >(tee -a $LOGS_DIR/$INSTALL_LOG_FILE) 2>&1
    log "  > OK" green
    log "- Enable apache2 site: sango"
    sudo a2ensite sango > >(tee -a $LOGS_DIR/$INSTALL_LOG_FILE) 2>&1
    log "  > OK" green
    log "- Enable apache2 rewrite module"
    sudo a2enmod rewrite > >(tee -a $LOGS_DIR/$INSTALL_LOG_FILE) 2>&1
    log "  > OK" green
    log "- Restart apache2"
    sudo systemctl reload apache2 > >(tee -a $LOGS_DIR/$INSTALL_LOG_FILE) 2>&1
    log "  > OK" green

    # Give permissions
    log "- Give permissions to /mystation/server"
    sudo chown -R admin:www-data /mystation/server > >(tee -a $LOGS_DIR/$INSTALL_LOG_FILE) 2>&1
    sudo chmod -R 770 /mystation/server > >(tee -a $LOGS_DIR/$INSTALL_LOG_FILE) 2>&1
    log "  > OK" green

    # Declare 'mystation' command alias
    log "- Define 'mystation' command alias"
    echo "alias mystation='node /mystation/bin/cli.js'" >> ~/.bash_aliases > >(tee -a $LOGS_DIR/$INSTALL_LOG_FILE) 2>&1
    log "  > OK" green

    # Change wallpaper
    log "- Change wallpaper"
    sudo cp /mystation/install/desktop-background/desktop-items-0.conf /etc/xdg/pcmanfm/LXDE-pi/desktop-items-0.conf > >(tee -a $LOGS_DIR/$INSTALL_LOG_FILE) 2>&1
    sudo cp /mystation/install/desktop-background/desktop-items-1.conf /etc/xdg/pcmanfm/LXDE-pi/desktop-items-1.conf > >(tee -a $LOGS_DIR/$INSTALL_LOG_FILE) 2>&1
    log "  > OK" green

    # Configure chromium autostart to display UI
    log "- Configure chromium autostart"
    sudo chmod 777 /etc/xdg/lxsession/LXDE-pi/autostart > >(tee -a $LOGS_DIR/$INSTALL_LOG_FILE) 2>&1
    sudo echo "/usr/bin/chromium-browser --kiosk http://localhost/" >> /etc/xdg/lxsession/LXDE-pi/autostart > >(tee -a $LOGS_DIR/$INSTALL_LOG_FILE) 2>&1
    sudo chmod 644 /etc/xdg/lxsession/LXDE-pi/autostart > >(tee -a $LOGS_DIR/$INSTALL_LOG_FILE) 2>&1
    log "  > OK" green

    # Show version
    log "- Check version"
    node /mystation/bin/cli.js --version > >(tee -a $LOGS_DIR/$INSTALL_LOG_FILE) 2>&1
    log "  > OK" green

    log "Installation finished. Please reboot the system." yellow

    log "---------------------------------" blue
    log "| > End $(date '+%Y-%m-%d %H:%M:%S')     |" blue
    log "---------------------------------" blue

fi
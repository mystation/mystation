# MyStation

Check the documentation at: [http://developers.mystation.fr/](http://developers.mystation.fr/)

## Get started

```bash
# Clone the repository
git clone https://gitlab.com/mystation/mystation.git
# Initialize
npm run init
```

> In raspbian environment, you must probably to create folder and give access before cloning:
> ```bash
> sudo mkdir /mystation
> sudo chown -R pi:pi /mystation
> cd /mystation
> git clone https://gitlab.com/mystation/mystation.git . # install in /mystation
> ```

## Configuration

> See `config/config.default.json`

```json
{
    "database": {
        "host": "localhost",
        "port": 3306,
        "database": "mystation"
    },
    "mystation-web-api": {
        "url": "http://mystation.fr/api"
    }
}
```

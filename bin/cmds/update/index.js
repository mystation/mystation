/**
 * MyStation - `update` command
 */

const childp = require('child_process')
const path = require('path')
const axios = require('axios')

const { config, rootPath, version, wait } = require('../../utils/')

const VERSION = version.global

// Exports yargs command as module

exports.command = 'update'

exports.aliases = [/* no aliases */]

exports.describe = 'Update MyStation'

exports.builder = function (yargs) {
  yargs
    .usage('Usage: $0 update [--force]')
    .option('force', {
      alias: 'f',
      type: 'boolean',
      description: 'Force update'
    })
}

exports.handler = async function (argv) {
  const forceMode = argv.force || false
  console.log((`--------------------------`).cyan)
  console.log((`Update : \n`).cyan)
  let lastVersion
  await wait(`Request MyStation official API server...\n\n`, async () => {
    const response = await axios.get(config['mystation-web-api'].url + '/version/current')
    lastVersion = response.data
  })
  console.log('MyStation last version: ', lastVersion.version + '\n')
  console.log('Compare with local version: ', `${VERSION}\n`)
  if (!forceMode && VERSION === lastVersion.version) {
    console.log('Already up to date!\n'.yellow)
  } else {
    // childp.execSync(`git checkout tags/v${lastVersion.version} --recurse-submodules && bash ./install/update.sh`, { cwd: rootPath(), stdio: 'inherit' })
    childp.execSync(`git submodule foreach git checkout -- . && git pull --recurse-submodules && bash ./install/update.sh`, { cwd: rootPath(), stdio: 'inherit' })
    console.log('> OK')
  }
  process.exit(0)
}
/**
 * MyStation - `admin` command
 */

const childp = require('child_process')
const path = require('path')

const { config, rootPath, wait } = require('../../utils/')

// Exports yargs command as module

exports.command = 'admin [change-password] <password>'

exports.aliases = [/* no aliases */]

exports.describe = 'Available options: --change-password'

exports.builder = {
  // Option: --change-password
  'change-password': {
    type: 'boolean'
  }
}

exports.handler = function (argv) {
  console.log((`--------------------------`).cyan)
  console.log((`Admin : \n`).cyan)
  console.log((`WARNING, this command reset only system user \`admin\` password. Please prefer change password via User Interface > Settings > Reset admin password (must be authenticated as admin).\n`).yellow)
  // Change password sub-command
  if (argv.changePassword) {
    const passwdValue = argv.password
    if (passwdValue && typeof passwdValue === 'string') {
      console.log((`- Change password... \n`).cyan)
      childp.execSync(`sudo usermod --password $(openssl passwd -1 '${passwdValue}') admin`, { stdio: 'inherit' })
      console.log('> OK')
      process.exit(0)
    } else {
      console.log('Error: password must be string'.red)
      console.log('Usage: admin --change-password \'password\'')
      process.exit(1)
    }
  } else {
    console.log('Error for \`admin\` command: option missing'.red)
    console.log('Usage: admin --change-password \'password\'')
    process.exit(1)
  }
}

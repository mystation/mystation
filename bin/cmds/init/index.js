/**
 * MyStation - `init` command
 */

const childp = require('child_process')
const fs = require('fs')
const path = require('path')

const { config, rootPath, wait } = require('../../utils/')

// Exports yargs command as module

exports.command = 'init [dev]'

exports.aliases = [/* no aliases */]

exports.describe = 'Initialize MyStation'

exports.builder = {
  // Option: --dev
  dev: {
    type: 'boolean'
  }
}

exports.handler = function (argv) {
  const devMode = argv.dev || false
  console.log((`--------------------------`).cyan)
  console.log((`Initialize : \n`).cyan)
  // First, check if config file exists
  if (!fs.existsSync(rootPath('/config/config.json'))) {
    fs.copyFileSync(rootPath('/config/config.default.json'), rootPath('/config/config.json'))
  }
  if (devMode) {
    // App development kit
    childp.execSync(`git submodule init adk`, { cwd: rootPath(), stdio: 'inherit' })
    childp.execSync(`git submodule update --remote --merge adk`, { cwd: rootPath(), stdio: 'inherit' })
    childp.execSync(`cd adk && git checkout master`, { cwd: rootPath(), stdio: 'inherit' })
    // No install for adk...
    // User interface
    childp.execSync(`git submodule init ui`, { cwd: rootPath(), stdio: 'inherit' })
    childp.execSync(`git submodule update --remote --merge ui`, { cwd: rootPath(), stdio: 'inherit' })
    childp.execSync(`cd ui && git checkout master`, { cwd: rootPath(), stdio: 'inherit' })
    childp.execSync(`cd ui && npm install`, { cwd: rootPath(), stdio: 'inherit' })
  }
  // Database
  childp.execSync(`git submodule init database`, { cwd: rootPath(), stdio: 'inherit' })
  childp.execSync(`git submodule update --remote --merge database`, { cwd: rootPath(), stdio: 'inherit' })
  childp.execSync(`cd database && git checkout master`, { cwd: rootPath(), stdio: 'inherit' })
  // Server
  childp.execSync(`git submodule init server`, { cwd: rootPath(), stdio: 'inherit' })
  childp.execSync(`git submodule update --remote --merge server`, { cwd: rootPath(), stdio: 'inherit' })
  childp.execSync(`cd server && git checkout master`, { cwd: rootPath(), stdio: 'inherit' })
  childp.execSync(`cd server && composer initialize`, { cwd: rootPath(), stdio: 'inherit' })
  // OK
  console.log('> OK')
  process.exit(0)
}
#!/usr/bin/env node

/*

                   /#                   
               ##########(              
           ######       ######          
      /#####(               ######*     
  ######/                       *###### 
/*****.                           ,*****
(((*,******                   ******,(((    ___  ___      _____ _        _   _               _____  _     _____  
(((*     ******          .******     (((    |  \/  |     /  ___| |      | | (_)             /  __ \| |   |_   _| 
(((*         ******, ,*****,         (((    | .  . |_   _\ `--.| |_ __ _| |_ _  ___  _ __   | /  \/| |     | |   
(((*              *****#             (((    | |\/| | | | |`--. \ __/ _` | __| |/ _ \| '_ \  | |    | |     | |   
(((*               ***######.        (((    | |  | | |_| /\__/ / || (_| | |_| | (_) | | | | | \__/\| |_____| |_  
(((*               ***    #######    (((    \_|  |_/\__, \____/ \__\__,_|\__|_|\___/|_| |_|  \____/\_____/\___/  
(((*               ***        ,######(((             __/ |                                                       
/(((.              ***             ##(((            |___/
  /######          ***          ######. 
       ######,     ***     ,#####(      
           ######( *** ######/          
               ,###***###              

*/

'use strict'

const yargonaut = require('yargonaut') // yargonaut first
const colors = require('colors')
const path = require('path')
const fs = require('fs')

const { config, rootPath, version, wait } = require('./utils/')

const VERSION = version.global
const SERVER_VERSION = version.server
const UI_VERSION = version.ui

const versionOutput = `VERSION:\n
MyStation v${VERSION}
├─ server: ${SERVER_VERSION}
└─ ui: ${UI_VERSION}
`

console.log((`
--------------------------
MyStation CLI
--------------------------`).cyan)

console.log('Made with ' + (('❤').red) + '  by ' + 'Hervé Perchec <herve.perchec@gmail.com>'.yellow)
console.log('See also ' + `http://mystation.fr/`.yellow)
console.log((`--------------------------\n`).cyan)

yargonaut.style('yellow')
  .helpStyle('cyan.underline')

var yargs = require('yargs')
  .scriptName('mystation')
  .usage('$0 <command> | --version | --help')
  .command(require('./cmds/admin/'))
  .command(require('./cmds/init/'))
  .command(require('./cmds/update/'))
  .version(versionOutput)
  .help()

const commands = yargs.getCommandInstance().getCommands()

var argv = yargs.argv

// Check if command is set
if (!argv._[0] || commands.indexOf(argv._[0]) === -1) {
  console.log(('Error : "' + argv._[0] + '" - non-existing or no command specified\n').red)
  yargs.showHelp()
  process.exit(1)
}



// // Global config
// // const GLOBAL_CONFIG = require('../config/config.json')
// // Optimist
// var optimist = require('optimist')
// // Database manager
// // const DatabaseManager = require('../utils/database-manager')
// // var DBManager = new DatabaseManager(GLOBAL_CONFIG.database)
// // Commands
// const initCmd = require('./cmds/init/index.js')
// const adminCmd = require('./cmds/admin/index.js')
// // argv
// // See optimist package documentation 
// var argv = optimist.parse(process.argv.slice(2)) // start by: '/usr/bin/node', '/mystation/cli/cli.js', ...
// // Get commands
// const commands = argv._

// // Declare processes
// var processes;

// /**
//  * Extract options property
//  * @param {Object} args - Optimist argv object
//  * @return {Object}
//  */
// function extractOptions (args) {
//   var tmp = {}
//   Object.keys(args).forEach(prop => {
//     if (prop !== '_' && prop !== '$0') {
//       tmp[prop] = args[prop]
//     }
//   })
//   return tmp
// }

// /**
//  * Describe shared
//  */
// function describeShared () {
//   optimist
//     .usage('MYSTATION\n\n' +
//       'Usage:\n' +
//       '  $0 <command>\n\n' +
//       'Commands:\n' +
//       '  init [--dev] Initialize MyStation.\n' +
//       '  boot [<options>] Boot MyStation.\n' +
//       '  prun <processName> [<options>] Run a process.')
//     .describe('help', 'Print usage and options.')
//     .describe('version', 'Print current version.')
//   optimist.showHelp()
// }

// /**
//  * Describe version
//  */
// function describeVersion () {
//   optimist
//     .usage('MYSTATION\n\n' +
//       'Version:\n' +
//       '  ' + GLOBAL_CONFIG.version + '\n\n' +
//       'Made with love by Hervé Perchec <herve.perchec@gmail.com>\n' +
//       'http://www.mystation.fr/')
//     .describe('help', 'Print usage and options.')
//     optimist.showHelp()
// }

// /**
//  * Process args
//  * @param {array} argv - Array of commands
//  * @param {Object} options - Command options
//  */
// function processArgs (argv, options) {
//   // If an option is passed, don't interpret command -> exit after describe
//   // If --help
//   if (options.help) {
//     describeShared()
//     process.exit(0) // exit
//   }
//   // If --version
//   if (options.version) {
//     describeVersion()
//     process.exit(0) // exit
//   }

//   // Scan commands
//   const rootCommand = argv[0] || null
//   if (!rootCommand) { // No command...
//     // Show help and exit
//     describeShared()
//     process.exit(0) // exit
//   } else {
//     switch (rootCommand) {
//       case 'init':
//         initCmd(options.dev)
//         process.exit(0) // exit
//         break
//       case 'boot': 
//         console.log('Ok, vous voulez booter mystation')
//         process.exit(0) // exit
//         break
//       case 'admin:change_password': 
//         adminCmd(argv[1]) // pass 1st arg
//         process.exit(0) // exit
//         break
//       case 'prun':
//         // Check if process name exists 
//         if (!argv[1]) {
//           console.error('Missing process name')
//           console.log('Available processes:')
//           processes.forEach((p) => {
//             console.log('- ' + p.name)
//           })
//           console.log('Use: "mystation prun <processName>"')
//           console.log('Run "mystation --help" to see usage.')
//           process.exit(1) // Exit with error
//         } else {
//           // Find process
//           let tmpProcess = processes.find((process) => {
//             return process.name === argv[1]
//           })
//           if (!tmpProcess) {
//             console.error('Unknown process "' + argv[1] + '".')
//             console.log('Available processes:')
//             processes.forEach((p) => {
//               console.log('- ' + p.name)
//             })
//             console.log('Use: "mystation prun <processName>"')
//             console.log('Run "mystation --help" to see usage.')
//             process.exit(1) // Exit with error
//           } else {
//             console.log(tmpProcess)
//             process.exit(0) // exit
//           }
//         }
//         break
//       default:
//         console.error('Unknown command "' + rootCommand + '".')
//         console.log('Run "mystation --help" to see usage.')
//         process.exit(1) // Exit with error
//     }
//   }
// }

// const run = async () => {
//   // console.log(argv)
//   // console.log(commands)
//   // console.log(extractOptions(argv))
//   // Init database connection
//   // DBManager.init()
//   // // Get processes
//   // processes = await DBManager.query('SELECT * FROM processes')
//   // Process
//   processArgs(commands, extractOptions(argv))
// }

// run()
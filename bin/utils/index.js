const path = require('path')
const fs = require('fs')

const loading = {
    // Duration of each character displaying (in ms)
    velocity: 250,
    // Reference to call setInterval and clearInterval
    intervalRef: null,
    // Display loading to STDOUT
    display: function (message) {
        var P = ["\\", "|", "/", "-"]
        var S = ["   ", ".  ", ".. ", "..."]
        var x = 0
        var y = 0
        var velocity = this.velocity
        this.intervalRef = setInterval(function () {
            process.stdout.write("\r" + P[x++] + ' - ' + message + ' : ' + S[y++])
            x &= 3
            y &= 3
        }, velocity)
    },
    stop: function() {
        clearInterval(this.intervalRef)
        // Wrap in try/catch block because process.stdout.clearLine (or cursorTo) is non-TTY
        // This causes an error if command is executed by php or npm (for example)
        try {
          process.stdout.clearLine()
          process.stdout.cursorTo(0)
        } catch(err) {
          console.log('Warn: process.stdout.clearLine (or cursorTo) function is non-TTY. Skipped...')
        }
        
    }
}

const rootPath = (targetPath = '/') => path.join(__dirname, '../../', targetPath)

// Export config
module.exports.config = fs.existsSync(rootPath('/config/config.json')) 
    ? require('../../config/config.json')
    : require('../../config/config.default.json')
// Export rootPath helper
module.exports.rootPath = rootPath
// Export local version
module.exports.version = {
  global: fs.readFileSync(rootPath('/VERSION'), { encoding: 'utf8' }),
  server: fs.existsSync(rootPath('/server/VERSION'))
    ? fs.readFileSync(rootPath('/server/VERSION'), { encoding: 'utf8' })
    : '<unknown>',
  ui: fs.existsSync(rootPath('/server/public/VERSION'))
    ? fs.readFileSync(rootPath('/server/public/VERSION'), { encoding: 'utf8' })
    : '<unknown>'
}
// Export wait method to display loading in stdout
module.exports.wait = async function (message, fct) {
  loading.display(message)
  await fct()
  loading.stop()
}

